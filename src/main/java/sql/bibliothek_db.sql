create database Bibliothek_DB;

Use Bibliothek_db;

create table Bibliothek 
(
  bib_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
, adr_id int not null
, name VARCHAR (20) NOT NULL
, webadresse varchar(50)
);

create table Buecher_In_Bibliothek 
(
  bubib_id int not null auto_increment primary key
, bib_id int not null
, bu_id int not null
);

create table reservation 
(
  res_id int not null auto_increment primary key
, kunde_id int not null
, bubib_id int not null
, datum timestamp not null default current_timestamp
, status bit
);


create table Journal 
(
  journal_id int not null auto_increment primary key
, kunde_id int not null
, bubib_id int not null
, ausleih timestamp not null default current_timestamp
, rückgabe date
);

create table Buecher 
(
  Bu_id int not null auto_increment primary key
, buAu_id int not null
, bubib_id int not null
, titel VARCHAR(50) not null
, seiten int 
, ISBN int not null
, jahr date
, genere VARCHAR(20)
);

create table Buchautor 
(
  BuAu_id int not null auto_increment primary key
, bu_id int not null
, autor_id int not null
);

create table Autoren 
(
  Autor_id int not null auto_increment primary key
, pers_id int not null
);

create table Kunden 
(
  kunde_id int not null auto_increment primary key
, pers_id int not null
);

create table Person 
(
  pers_id int not null auto_increment primary key
, adr_id int not null
, first_name varchar(20) not null
, last_name varchar(20) not null
, tel int
, email varchar(50) not null
);

create table address
(
  adr_id int not null auto_increment primary key
, strasse varchar(30)
, ort varchar(20)
, land varchar(20)
);

-- Fremschlüsseln einfügen
-- ------------------------------------------------------------------------

alter table Bibliothek 
  ADD FOREIGN KEY (adr_id) REFERENCES address(adr_id)
;

alter table Buecher_In_Bibliothek 
  ADD FOREIGN KEY (bib_id) REFERENCES Bibliothek(bib_id)
, ADD FOREIGN KEY (bu_id) REFERENCES Buecher(bu_id)
;

alter table reservation 
  ADD FOREIGN KEY (kunde_id) REFERENCES Kunden(kunde_id)
, ADD FOREIGN KEY (bubib_id) REFERENCES Buecher_In_Bibliothek(bubib_id)
;

alter table Journal 
  ADD FOREIGN KEY (kunde_id) REFERENCES Kunden(kunde_id)
, ADD FOREIGN KEY (bubib_id) REFERENCES Buecher_In_Bibliothek(bubib_id)
;

alter table Buecher 
  ADD FOREIGN KEY (BuAu_id) REFERENCES BuchAutor(BuAu_id)
, ADD FOREIGN KEY (bubib_id) REFERENCES Buecher_In_Bibliothek(bubib_id)
;

alter table BuchAutor 
  ADD FOREIGN KEY (Bu_id) REFERENCES Buecher(Bu_id)
, ADD FOREIGN KEY (autor_id) REFERENCES Autoren(autor_id)
;

alter table Autoren 
  ADD FOREIGN KEY (pers_id) REFERENCES person(pers_id)
;

alter table kunden 
  ADD FOREIGN KEY (pers_id) REFERENCES person(pers_id)
;

alter table person 
  ADD FOREIGN KEY (adr_id) REFERENCES address(adr_id)
;
