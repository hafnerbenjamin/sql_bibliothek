package main;

/**
 * Created by Benj on 02.06.2016.
 */

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class BookOverviewController {
    @FXML
    public static TableView<Book> bookTable;
    @FXML
    private TableColumn<Book, String> booksColumn;

    @FXML
    private Label titleLabel;
    @FXML
    private Label authorLablel;
    @FXML
    private Label pagesLabel;
    @FXML
    private Label isbnLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label yearLabel;

    // Reference to the main application.
    private Main main;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public BookOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the person table with the two columns.
        booksColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
    }


}