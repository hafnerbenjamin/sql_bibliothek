package main;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Model class for a Person.
 *
 * @author Marco Jakob
 */
public class Book {

    private final StringProperty title;
    private final StringProperty autor;
    private final IntegerProperty pages;
    private final IntegerProperty isbn;
    private final StringProperty city;
    private final IntegerProperty year;


    public Book(String title, String autor) {
        this.title = new SimpleStringProperty(title);
        this.autor = new SimpleStringProperty(autor);

        // Some initial dummy data, just for convenient testing.
        this.pages = new SimpleIntegerProperty(1);
        this.isbn = new SimpleIntegerProperty(1234);
        this.city = new SimpleStringProperty("some city");
        this.year = new SimpleIntegerProperty(2000);
    }

    public String getTitle() {
        return title.get();
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public StringProperty titleProperty() {
        return title;
    }

    public String getAutor() {
        return autor.get();
    }

    public void setAutor(String autor) {
        this.autor.set(autor);
    }

    public StringProperty autorProperty() {
        return autor;
    }

    public int getPages() {
        return pages.get();
    }

    public void setPages(int pages) {
        this.pages.set(pages);
    }

    public IntegerProperty pagesProperty() {
        return pages;
    }

    public int getIsbn() {
        return isbn.get();
    }

    public void setIsbn(int isbn) {
        this.isbn.set(isbn);
    }

    public IntegerProperty isbnProperty() {
        return isbn;
    }

    public String getCity() {
        return city.get();
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public StringProperty cityProperty() {
        return city;
    }

    public int getYear() {
        return year.get();
    }

    public void setYear(int year) {
        this.year.set(year);
    }

    public IntegerProperty yearProperty() {
        return year;
    }
}